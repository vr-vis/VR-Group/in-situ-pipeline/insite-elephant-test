import numpy as np
from mpi4py import MPI

from timer import MultiTimer

last_package = [None] * 3125

def store_and_distribute(comm, data_comm, ana_comm, simdata=None, anadata=None, mode='a'):
    '''
    Creates the intracommunicator for parallel data transfer.
    Creates the shared memory block and buffer.
    Rank 0: receiving data into the buffer
    Rank 1-x: parallel data transfer (append or sort mode)
    
    :param comm: Intracommunicator, will be split into rank 0 and rank 1-x
    :param data_comm: Intercommunicator between server and simulation
    :param ana_comm: Intercommunicator between server and analysis
    :param simdata: Parameters from simulation. Currently: size of the packages sent.
    :param anadata: Parameters from analysis. Currently: number of neurons to analyze.
    :param mode: Transposition mode. 'a' = append-algorithm, 's' = sort-algorithm
    '''

    ### NOTE: known information in simdata and anadata
    sizes = simdata[0]  # number of lines = packages size
    max_neurons = simdata[1]  # simulation size = max number of neurons
    nn = anadata  # number of neurons requested by analysis
    MultiTimer(f"transfer {nn} neurons")

    # new server-intracommunicator without receiving rank 0
    intracomm = comm.Create(comm.Get_group().Excl([0]))

    # create the shared memory block / databuffer
    buf_pkgs = 100  # NOTE: size hardcoded!
    databuffer = _shared_mem_buffer(buf_pkgs, sizes, comm)
    MultiTimer("databuffer")

    # rank 0: receive and fill buffer
    if comm.Get_rank() == 0:
        _fill_buffer(data_comm, databuffer, sizes, buf_pkgs)
    # rank 1-x: transfer to analysis
    else:
        _transfer_to_analysis(mode, databuffer, ana_comm, intracomm, nn, max_neurons)

    print(f"rank {comm.Get_rank()} finished")
    MultiTimer("end").print_timings()


### all sending stuff
def _send_to_analysis(res, ana_comm):
    global last_package
    for i,d in enumerate(res):
        if last_package[i] != None:
            res[i] = res[i][res[i] > last_package[i]]
        if res[i].size > 0:
            last_package[i] = res[i].max()
    ana_comm.send(res, dest=0, tag=11)
    MultiTimer("send")


def _end_of_sim(ana_comm, rank):
    if rank == 0:
        ana_comm.send(None, dest=0, tag=10)


def _shared_mem_buffer(buf_pkgs, sizes, comm):
    '''
    Create a shared memory block.
    MPI One-sided-Communication.
    Here: fixed size for a NEST simulation output:
    number of packages in buffer * two doubles * number of lines(from all ranks) 
    '''
    pkg_size = np.sum(sizes)
    print(f"pkg_size: {pkg_size}")
    datasize = MPI.DOUBLE.Get_size()
    if comm.Get_rank() == 0:
        bufbytes = buf_pkgs * datasize * 2 * pkg_size
    else:
        bufbytes = 0
    # rank 0: create the shared block
    # rank 1-x: get a handle to it
    win = MPI.Win.Allocate_shared(bufbytes, datasize, comm=comm)
    buf, datasize = win.Shared_query(0)
    assert datasize == MPI.DOUBLE.Get_size()
    # create a numpy array (buffer) whose data points to the shared mem
    return np.ndarray(buffer=buf, dtype='d', shape=(buf_pkgs*pkg_size,2))


def _transfer_to_analysis(mode, databuffer, ana_comm, intracomm, nn, max_neurons):
    '''
    Transfer data to analysis.
    --> transpose nest output in parallel (append or sort algorithm)
    --> data exchange with MPI
    --> sends out neuron spiketimes (spike trains) as numpy arrays
    --> sends to analysis via intercommunicator
    
    :param databuffer: Handle to the databuffer
    :param ana_comm: Intercommunicator between server and analysis
    :param intracomm: The new intracommunicator, without the receiving rank
    :param nn: number of neurons to analyze
    :param max_neurons: size of the simulation
    '''
    size = intracomm.Get_size()
    rank = intracomm.Get_rank()

    if mode == 'a':
        print(f'Transferring data, transpose mode: append')
        MultiTimer("Transpose: append")
        try:
            while (True):
                # 1) transpose
                transposed = _transpose_append(databuffer, ana_comm, intracomm, size, rank, max_neurons)

                # 2) exchange data, comp result
                result = _exchange_data_comp_results(transposed[:nn], intracomm, size, rank)
                MultiTimer("convert data")

                # 3) send
                _send_to_analysis(result, ana_comm)
        except EndOfSimException:
            pass

    elif mode == 's':
        print(f'Transferring data, transpose mode: sort')
        MultiTimer("Transpose: sort")
        try:
            while (True):
                # 1) transpose
                transposed = _transpose_sort(databuffer, ana_comm, intracomm, size, rank, nn)

                # 2) exchange data, comp result
                result = _exchange_data_comp_results(transposed[:nn], intracomm, size, rank)
                MultiTimer("convert data")

                # 3) send
                _send_to_analysis(result, ana_comm)
        except EndOfSimException:
            pass


def _transpose_append(databuffer, ana_comm, intracomm, size, rank, max_neurons):
    '''
    Transpose data with the append-algorithm.
    '''
    # NOTE: index = Neurons ID, index -1 for zeros in buffer (neuron id 1001)
    transposed = [list() for _ in range(max_neurons + 1)]  # +1 for empty-placeholder
    MultiTimer("init lists")

    data_to_transpose = np.array_split(databuffer, size)[rank]  # buffer access
    intracomm.Barrier()  # check for end of sim.
    if np.isnan(databuffer[0, 0]):
        _end_of_sim(ana_comm, rank)
        raise EndOfSimException
    MultiTimer("data")

    for d in data_to_transpose:
        transposed[int(d[0]) - 1].append(d[1])
    MultiTimer("transpose")

    return transposed


def _transpose_sort(databuffer, ana_comm, intracomm, size, rank, nn):
    '''
    Transpose data with the sort-algorithm.
    '''
    rank_data = np.array_split(databuffer, size)[rank]  # buffer access
    intracomm.Barrier()  # check for end of sim.
    if np.isnan(databuffer[0, 0]):
        _end_of_sim(ana_comm, rank)
        raise EndOfSimException
    MultiTimer("data")

    data_to_transpose = rank_data[rank_data[:, 0].argsort()]
    MultiTimer("sort")

    neuron_id, counts = np.unique(
        data_to_transpose[:, 0], return_counts=True)  # get IDs and number of occurences (spiketimes)
    transposed = np.split(  # split sorted data...
        data_to_transpose[:, 1],  # along second column...
        np.cumsum(counts)[:-1])  # where first column has index/neuron id change
    MultiTimer("transpose")

    ### this reshaping does: 
    # 1) handle zeros from empty buffer (before first filling)
    # 2) insert empty arrays where no spikes occured
    if neuron_id.shape[0] != 1:  # if there are only zeros -> shape= (1,) do nothing
        if neuron_id[0] == 0.0:
            neuron_id = neuron_id[1:]
            del transposed[0]
    for idx in _missing_elements(neuron_id[:nn], nn):
        transposed.insert(idx - 1, np.array([]))
    MultiTimer("reshape")

    return transposed


def _exchange_data_comp_results(ana_neurons, intracomm, size, rank):
    '''
    Does the data redistribution to the ranks, reshaping and sending,
    e.g. nn = 100, size = 7 -> [15, 15, 14, 14, 14, 14, 14].
    One Gatherv on each MPI rank.
    
    :param ana_neurons: the neuron spiketimes for analysis
    :param intracomm: the intracommunicator for data exchange
    :param size, rank: number of ranks and rank id
    '''

    datashape = [len(i) for i in ana_neurons]  # shape of transposed-'matrix'
    recvbuf = None
    sendcounts = None
    for root in range(size):
        # gather number of spiketimes of the neurons from all ranks (info about index, where to split later)
        flatcounts = intracomm.gather(np.array_split(datashape, size)[root], root)
        if rank == root:
            tmp = flatcounts  # save on each rank
            # Receive buffer: 1D-array of ALL spiketimes (all neurons per rank, all ranks)
            recvbuf = (np.empty(np.sum(np.concatenate(flatcounts)), dtype=np.float64),
                       np.sum(flatcounts, axis=1))  # (recvbuf,sendcounts) for gatherv

        # Send Buffer: the neurons from each rank flattened and all concatenated
        intracomm.Gatherv(sendbuf=np.concatenate(np.array_split(ana_neurons, size)[root]),
                          recvbuf=recvbuf,
                          root=root)
        MultiTimer("gatherv")

    # split at flatcounts indices
    data = np.split(recvbuf[0], np.cumsum(tmp))[:-1]
    b = len(data) // size
    # concatenate all b corresponding rows (every b-th) and stak them
    return [np.concatenate([data[i + j] for i in range(0, len(data), b)]) for j in range(b)]

def spike_datatype():
    structtype = MPI.Datatype.Create_struct([1, 1], [0, 8], [MPI.DOUBLE, MPI.DOUBLE])
    structtype.Commit()
    return structtype

def _fill_buffer(data_comm, databuffer, sizes, buf_pkgs):
    '''
    Receive data, put it into ring buffer
    '''
    head_ = None
    pkg_counter = 0
    sending_ranks = len(sizes)  # number of ranks sending packages
    sizes_counter = 0
    status_ = MPI.Status()

    mytype = spike_datatype()

    MultiTimer("Databuffer: start receive")
    while (True):
        # receive 'current sizes index' number of lines from simulation
        head_ = sizes[sizes_counter] * pkg_counter
        MultiTimer("Databuffer: move head")

        # fill buffer at correct position  (offset)
        # source is the current sending simulation rank (sizes_counter)
        #print("Waiting for data")
        data_comm.Recv([databuffer[head_:],MPI.DOUBLE], source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status_)
        #print(databuffer[head_:head_+sizes[sizes_counter]])
        MultiTimer("Databuffer: recv+fill")

        if status_.tag == 113:
            print(f"Databuffer: received last package from simulation")
            # TODO: head of buffer information and end of simulation information
            # -> use one-sided-communication for async processing
            databuffer[0, 0] = None  # mark first buffer entry: END of simulation
            break
        sizes_counter = (sizes_counter + 1) % sending_ranks  # cycle through sizes array
        pkg_counter = (pkg_counter + 1) % buf_pkgs  # cycle through buffer


def _missing_elements(L, nn):
    '''
    From the first nn entries in L (neuron ids):
    which are missing (didn't fire)
    '''
    return sorted(set(range(nn + 1)).difference(L))


### break while loop from inside the transpose function
class EndOfSimException(Exception): pass


# old...for testing/validation...
def _print_info(data):
    datashape = [len(i) for i in data]
    print(f" Shape of the transposed data: {datashape} ")  # length of subarrays
    print(f" Number of neurons: {len(datashape)}")  # number of subarray
    print(f" Number of events (total): {sum(datashape)}")  # total size (number of elements)
