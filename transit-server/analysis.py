import sys
import os
from mpi4py import MPI
import numpy as np
from PluginManagement import PluginManagement
import time
from timer import MultiTimer
from IntercommManager import IntercommManager
import psycopg2
import logging
import multiprocessing
from mpi4py.futures import MPIPoolExecutor

class AppFilter(logging.Filter):
    rank = 0

    def __init__(self):
        self.rank = MPI.COMM_WORLD.Get_rank()

    def filter(self, record):
        record.mpi_rank = self.rank
        return True


ch = logging.StreamHandler(sys.stdout)
ch.setLevel(1)
ch.setFormatter(logging.Formatter('[%(filename)20s:%(lineno)4s - %(funcName)35s() - %(mpi_rank)s] %(message)s'))
ch.addFilter(AppFilter())
root = logging.getLogger()
root.setLevel(logging.DEBUG)
root.addHandler(ch)

logging.getLogger('yapsy').setLevel(logging.ERROR)
logging.getLogger('matplotlib').setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

# TODO: argument parsing/ error handling

neuron_one = np.empty(shape=(1, 1))

np.set_printoptions(precision=4)

plugin_manager = PluginManagement()

comm = MPI.COMM_WORLD

# request = None

intercomm = None

intercomm_to_access_node = IntercommManager()

processing_times = []

buffer_pos = np.zeros(dtype='i', shape=3130)


def prepare_shared_memory():
    logger.debug("Setting up shared memory")
    size = 3125

    itemsize = MPI.DOUBLE.Get_size()

    if comm.Get_rank() == 0:
        buffer_size = size * itemsize * 500
    else:
        buffer_size = 0

    win = MPI.Win.Allocate_shared(buffer_size, itemsize, comm=comm)

    shared_buff, itemsize = win.Shared_query(0)

    data_buffer = np.ndarray(buffer=shared_buff, dtype='d', shape=(size, 500))

    logger.debug("Waiting on comm Barrier")
    comm.Barrier()
    logger.debug("Comm Barrier cleared")
    return data_buffer


def get_port_from_db():
    database_host = 'database'
    con = psycopg2.connect(database='postgres', user='postgres', password='postgres', host=database_host, port='5432')
    cur = con.cursor()
    cur.execute("SELECT port FROM mpi_port_information WHERE port_type = 1")
    port = cur.fetchone()
    con.commit()
    cur.close()
    con.close()
    return port


def init_conn_to_transit_server(args):
    nn = int(args[1])  # e.g. 100

    ## Init connection
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    print(f"Waiting for port details")
    fport_path = "/tmp/anaport_in.txt"
    while not os.path.exists(fport_path):
        pass
    fport = open(fport_path, "r")
    port = fport.read()
    fport.close()
    print(f"Connecting to: {port}")
    ana_comm = comm.Connect(port, MPI.INFO_NULL, root=0)
    rank_ana = ana_comm.Get_rank()
    print(f"Connected, I'm rank: {rank_ana}\n")

    MultiTimer("analysis start")
    # if rank == 0:
    # ana_comm = ""
    # nn = 0
    if rank == 0:
        exchange_params(ana_comm, nn)
        receive_spiketimes(ana_comm, nn)

    # ana_comm =""
    # nn = 0

    # print(f'Analysis finished')
    # MultiTimer("ana end").print_timings()
    # MPI.Finalize( <= )


def exchange_params(ana_comm, nn):
    """
    send information, atm number of neurons to analyse..
    """
    ana_comm.send(nn, dest=0, tag=111)


def receive_spiketimes(ana_comm, nn):
    """
    Receive neuron spiketimes, start analysis
    """
    # global request
    status_ = MPI.Status()
    counter = 0
    request = None
    success = None
    call = 0

    data_from_node_two = []


    MultiTimer("ana init")
    print(f"Analysis: requesting data for {nn} neurons, start receiving...")
    time1_recv = time.time()
    data_buffer = prepare_shared_memory()
    while True:

        # intercomm_to_access_node.check_for_intercomm()

        if request is not None:
            success, data2 = request.test(status=status_)

        if request is None or success is True:
            request = ana_comm.irecv(800000, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
            # print(f"{MPI.COMM_WORLD.Get_rank()} received data from: {status_.source}")

        if success is True:
            call += 1
            if call == 1:
                print(len(data2))
            # print(neurons)
            # req = ana_comm.irecv(buf,source=MPI.ANY_SOURCE,tag=MPI.ANY_TAG)
            if status_.tag == 10:
                print(f"received last data package number: {counter + 1} from rank: {status_.source}")
                print(f"simulation ended, no new data to receive.")
            elif status_.tag == 11:
                # write_spikes_to_redis(data2, status_)
                write_spikes_to_shared_memory(data_buffer, data2, status_)
                if status_.source == 2:
                    data_from_node_two = data2.copy()

                if status_.source == 1:
                    with open('somefile.txt', 'a') as the_file:
                        the_file.write(f"idle e {(time.time())} {-(time.time() - time1_recv)*1000}\n")
                    print("Total time : %.1f ms" % (1000 * (time.time() - time1_recv)))
                    # data_buffer[0, 0] = data_buffer[0][0] + 1
                    data = data2 + data_from_node_two
                    comm.send(data, dest=1, tag=910)

                    #with open('somefile.txt', 'a') as the_file:
                        #the_file.write(f"idle s {time.time()}\n")
                    time1_recv = time.time()


def write_spikes_to_shared_memory(shared_memory, neurons, status):
    global buffer_pos
    time1 = time.time()
    number_of_neurons_in_pkg = len(neurons)
    if status.source == 1:
        offset = 0
        #print(neurons[0])
    else:
        offset = 1563
        # print(neurons[0])
    # print(f"Source: {status.source}")
    buffer_lpos = buffer_pos[offset:offset + number_of_neurons_in_pkg].tolist()
    new_sizes = [neuron.size for neuron in neurons]

    for r in range(number_of_neurons_in_pkg):

        if buffer_lpos[r] + neurons[r].size > shared_memory[r + offset].size:

            room_left_in_buffer = shared_memory[r + offset].size - buffer_lpos[r]
            shared_memory[r + offset][buffer_lpos[r]:shared_memory[r + offset].size] = neurons[r][: room_left_in_buffer]
            shared_memory[r + offset][:neurons[r].size - room_left_in_buffer] = neurons[r][room_left_in_buffer:]

        else:

            shared_memory[r + offset][buffer_lpos[r]:buffer_lpos[r] + neurons[r].size] = neurons[r]

    buffer_pos[offset:offset + number_of_neurons_in_pkg] = (buffer_pos[
                                                            offset:offset + number_of_neurons_in_pkg] + np.array(
        new_sizes)) % 500

    #print("Total time writing spikes: %.1f ms" % (1000 * (time.time() - time1)))
    # print(shared_memory[0])

    # for plugin in plugin_manager.plugin_objects:
    #    plugin.plugin_object.on_data(neuron_data)


def process_data():
    data_buffer = prepare_shared_memory()
    request = None
    status_ = MPI.Status()
    success = None
    while True:
        intercomm_to_access_node.check_for_intercomm()

        if request is not None:
            success, delta_data = request.test(status=status_)

        if request is None or success is True:
            request = comm.irecv(800000, source=0, tag=910)

        if success is True:
            jobs = []
            time1 = time.time()
            for plugin in plugin_manager.plugin_objects:
                if plugin.plugin_object.is_activated:
                    job = multiprocessing.Process(target=plugin.plugin_object.on_data, args=(data_buffer, delta_data))
                    jobs.append(job)
                    job.start()

            for job in jobs:
                job.join()
            print(f"Plugins took time : %.1f ms" % (1000 * (time.time() - time1)))


if __name__ == "__main__":

    intercomm_to_access_node.create_intercomm_connection()
    intercomm_to_access_node.plugin_manager = plugin_manager
    init_conn_to_transit_server(sys.argv)

    rank = MPI.COMM_WORLD.Get_rank()

    if rank == 0:
        pass

    if rank == 1:
        plugin_manager.load_plugins()
        plugin_manager.print_loaded_plugins()
        for plugin in plugin_manager.get_plugins():
            plugin.plugin_object.on_init()
        process_data()
