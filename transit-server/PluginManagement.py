from yapsy.PluginManager import PluginManager
from texttable import Texttable

class PluginManagement:
    plugin_directory = "plugins"
    manager = None
    plugin_objects = []


    def __init__(self):
        print("init")
        self.manager = PluginManager()

    def get_short_description(self,id):
        plugin, plugin_info = self.get_plugin(id)
        if plugin_info.details.has_option("Test", "Short Description"):
            return plugin_info.details.get("Test", "Short Description")
        else:
            return ""

    def get_long_description(self,id):
        plugin, plugin_info = self.get_plugin(id)
        if plugin_info.details.has_option("Test", "Long Description"):
            return plugin_info.details.get("Test", "Long Description")
        else:
            return ""

    def get_plugins(self):
        return self.plugin_objects

    def get_plugin(self,id):
        """

        :param id:
        :return: bla, blub
        """
        return self.plugin_objects[id].plugin_object, self.plugin_objects[id]

    def load_plugins(self):
        self.manager.setPluginPlaces([self.plugin_directory])
        self.manager.collectPlugins()
        for plugin in self.manager.getAllPlugins():
            self.plugin_objects.append(plugin)

    def print_loaded_plugins(self):
        print("Loaded plugins:")
        table = Texttable()
        table.set_deco(Texttable.HEADER | Texttable.VLINES | Texttable.BORDER)
        table.set_cols_align(["l","c"])
        table.set_cols_dtype(['t','i'])
        table.header(["ID","Name"])
        for i, plugin in enumerate(self.plugin_objects):
            table.add_row([i,plugin.name])

        print(table.draw())

    def call_function_on_plugin(self, plugin_id, func, *args):
        plugin_object, plugin_info = self.get_plugin(plugin_id)
        if callable(hasattr(plugin_object, func)) and callable(getattr(self, func)):
            print("Ambigous")
        elif hasattr(plugin_object, func):
            return getattr(plugin_object, func)(*args)
        elif hasattr(self, func):
            return getattr(self, func)(plugin_id, *args)
        else:
            print("No matching function found")

    def set_param(self, plugin_id, params):
        plugin_object, plugin_info = self.get_plugin(plugin_id)
        param_dict = params
        for key in param_dict:
            if hasattr(plugin_object, key):
                old_value = getattr(plugin_object, key)
                setattr(plugin_object, key, int(param_dict[key]))
                print(f"Changed the following parameter in {plugin_info.name}: {key}: {old_value} -> {param_dict[key]}")
            else:
                print(f"Tried to change {key} in {plugin_info.name} but key was not found")
        return self.get_plugin_param_values(plugin_id)

    def get_plugin_params(self, id):
        plugin, plugin_info = self.get_plugin(id)
        if plugin_info.details.has_option("Test", "Params"):
            return plugin_info.details.get("Test", "Params")
        else:
            return ""

    def get_plugin_param_values(self, id):
        string = self.get_plugin_params(id).splitlines()
        params = []
        description = []
        for line in string:
            params.append(line.split(":")[0])
            description.append(line.split(":")[1])

        plugin_object, plugin_info = self.get_plugin(id)
        response = []
        for i, param in enumerate(params):
            parameter_info = {}
            parameter_info['name'] = param
            parameter_info['value'] = getattr(plugin_object, param)
            parameter_info['description'] = description[i]
            response.append(parameter_info)

        return response

    def set_plugin_activation(self):
        pass

    def get_plugin_activated(self, id):
        response = {}
        plugin_object, plugin_info = self.get_plugin(id)
        response['active'] = plugin_object.is_activated

        return response

    def set_plugin_activated(self, id, new_state):
        response = {}
        plugin_object, plugin_info = self.get_plugin(id)
        if new_state is True:
            plugin_object.activate()
        elif new_state is False:
            plugin_object.deactivate()

        return self.get_plugin_activated(id)


    def get_plugin_details(self, id):
        response = {}
        plugin_object, plugin_info = self.get_plugin(id)
        response['name'] = plugin_info.name
        response['short description'] = self.get_short_description(id)
        response['long description'] = self.get_long_description(id)
        response['parameters'] = self.get_plugin_params(id)
        return response
