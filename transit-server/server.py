import sys
from mpi4py import MPI
import numpy as np
import psycopg2
import data_handler


# TODO: argument parsing/ error handling

def save_port_in_db(port_type, port):
    database_host = 'database'
    con = psycopg2.connect(database='postgres', user='postgres', password='postgres', host=database_host, port='5432')
    cur = con.cursor()
    cur.execute("DELETE FROM mpi_port_information WHERE port_type =%s", [port_type])
    cur.execute("INSERT INTO mpi_port_information(port_type,port) VALUES (%s,%s)", (port_type, port))
    con.commit()
    cur.close()
    con.close()

def main(args):
    arg1 = args[1]

    use_mpi = True
    # init MPI
    if use_mpi:
        comm = MPI.COMM_WORLD  # @UndefinedVariable
    else:
        comm = None

    ### Analysis: connection and information
    ana_comm, ana_port = analysis_connection(comm)

    ### Simulation: connection and information
    sim_comm, sim_port = simulation_connection(comm)

    mytype = spike_datatype()

    data = np.zeros(1, dtype=int)
    spike_data = np.zeros(100, dtype=([('time', np.double), ('gid', np.uint64)]))

    ### data/parameter exchange
    # data_from_ana, data_from_sim = data_exchange(comm,ana_comm,sim_comm)



    ### Data Handler: store data in buffer, distribute it to analysis
    sim_data = [[500,500], 3125 ]
    data_handler.store_and_distribute(comm, sim_comm, ana_comm, simdata=sim_data, anadata=3125, mode=arg1)

    ### finalize
    MPI.Close_port(ana_port)
    MPI.Close_port(sim_port)
    if comm.Get_rank() == 0:
        print(f'Port to analysis closed')
        print(f'Port to simulation closed')
        print(f'server end')
    MPI.Finalize()

"""


    while True:
        info = MPI.Status()
        sim_comm.Probe(status=info)
        count = info.Get_elements(MPI.DOUBLE)
        # count = 100
        print(f"Count: {count}")
        sim_comm.Recv([spike_data, count * 2, mytype], source=MPI.ANY_SOURCE, tag=14)
        print(spike_data)
"""

def spike_datatype():
    structtype = MPI.Datatype.Create_struct([1, 1], [0, 8], [MPI.DOUBLE, MPI.UINT64_T])
    structtype.Commit()
    return structtype


def data_exchange(comm, ana_comm, sim_comm):
    '''
    Exchanges data between simulation/analysis and the in transit server.
    '''

    # 1) analysis -> server
    ana_serv = None
    if ana_comm.Get_rank() == 0:
        ana_serv = ana_comm.recv(source=0, tag=111)
    data_from_ana = comm.bcast(ana_serv, root=0)

    # 2) simulation -> server
    sim_serv = None
    if sim_comm.Get_rank() == 0:
        sim_serv = sim_comm.recv(source=0, tag=111)
    data_from_sim = comm.bcast(sim_serv, root=0)

    return data_from_ana, data_from_sim


def analysis_connection(comm):
    if comm.Get_rank() == 0:
        ### Connection to analysis (outgoing data)
        print(f'trying to open MPI port')
        ana_port = MPI.Open_port(MPI.INFO_NULL)

        print(f"Writing port details")
        anaport_path = "/tmp/anaport_in.txt"
        fport = open(anaport_path, "w")
        fport.write(ana_port)
        fport.close()
        save_port_in_db(1, ana_port)
    else:
        ana_port = None

    # broadcast port info, accept connection on all ranks!
    ana_port = comm.bcast(ana_port, root=0)
    print(f"Rank {comm.Get_rank()} accepting connection on: {ana_port}\n")
    ana_comm = comm.Accept(ana_port, MPI.INFO_NULL, root=0)
    print(f"Analysis client connected to {ana_comm.Get_rank()}")

    return ana_comm, ana_port


def simulation_connection(comm):
    if comm.Get_rank() == 0:
        ### Connection to simulation (incoming data)
        print(f"open port for simulation")
        sim_port = MPI.Open_port(MPI.INFO_NULL)

        print(f"Writing port details")
        simport_path = "/tmp/simport_in.txt"
        fport = open(simport_path, "w")
        fport.write(sim_port)
        fport.close()
        save_port_in_db(2, sim_port)
    else:
        sim_port = None

    # broadcast port info, accept connection on all ranks!
    sim_port = comm.bcast(sim_port, root=0)
    print(f"Rank {comm.Get_rank()} accepting connection on: {sim_port}\n")
    sim_comm = comm.Accept(sim_port, MPI.INFO_NULL, root=0)
    print(f"Simulation client connected to {sim_comm.Get_rank()}")

    return sim_comm, sim_port


if __name__ == "__main__":
    main(sys.argv)
