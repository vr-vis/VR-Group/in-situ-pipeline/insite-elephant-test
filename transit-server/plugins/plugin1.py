import plugins.BasePlugin as BasePlugin
import numpy as np
from elephant.statistics import mean_firing_rate
import multiprocessing


class MyPlugin(BasePlugin.PluginOne):
    mfr_data = multiprocessing.Array('d', 3125, lock=False)
    max = multiprocessing.Value('d', 0)
    time_range = 500
    start_id = 20
    end_id = 30

    def on_init(self):
        self.activate()

    def print_name(self):
        print(f"MyPlugin1 activation status= {self.is_activated}")

    def on_data(self, data, delta_data):
        try:
            self.max.value = np.max(data)
            print(f"new max: {self.max.value}")
        except ValueError:
            pass

        for idx, neuron in enumerate(data):
            if np.count_nonzero(neuron) > 0:
                self.mfr_data[idx] = mean_firing_rate(neuron, t_start=self.max.value - self.time_range)

        return

    def get_data(self):
        # Prepare an empty dict for the response
        result = {}
        result['time_range'] = self.time_range
        result['timestamp'] = self.max.value

        # Prepare an empty dict for the neurons mfrs
        result['mfr'] = {}

        # Insert the mfr of the requested neuorons
        for i in range(self.start_id, self.end_id):
            result['mfr'][i] = self.mfr_data[i]

        # Return the result dict
        return result
