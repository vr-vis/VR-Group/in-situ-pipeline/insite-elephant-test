from yapsy.IPlugin import IPlugin
from abc import ABC,abstractmethod

class PluginOne(IPlugin,ABC):

    pluginInfo = None

    @abstractmethod
    def on_init(self):
        pass

    @abstractmethod
    def on_data(self, data, delta_data):
        pass

    @abstractmethod
    def get_data(self):
        pass