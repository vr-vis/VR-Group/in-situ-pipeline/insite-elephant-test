# Preliminary version of analysis to the insite pipeline

## Installation

1. Make sure docker is installed
2. Clone the repository
3. Update all submodules
4. Execute `docker-compose up --build`

Note: Due to database construction it may occur, that the pipeline will not run correctly when run for the first time. If this occurs simply stop the running pipeline with `Ctrl-C` and use `docker-compose up` again. Consecutive runs should run normally. 

## Usage

The API is available at `http://localhost:8080`.

Currently implemented API calls:


| METHOD  | URI  | EXAMPLE  |  
|---|---|---|
|  GET |  /analysis |   |   
|  GET | /analysis/PLUGIN_ID/   |   |   
|  GET | /analysis/PLUGIN_ID/active  |   |   |   |
|  PUT |  /analysis/PLUGIN_ID/active | analysis/0/active?new_state=true  |   
|  GET |  /analysis/PLUGIN_ID/params  |   |   |   |
|  PUT |  /analysis/PLUGIN_ID/params  | analysis/0/params?time_range=50  |   
|  GET |  /analysis/PLUGIN_ID/data |   |   |   |
|  GET |   /analysis/PLUGIN_ID/long_desc |   |   
|  GET | /analysis/PLUGIN_ID/short_desc  |   |   